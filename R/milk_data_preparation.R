# Preparation of the formula/breastfeeding variables variable
# M. Rolland
# 05/09/19

# milk data preparation: breastfeeding/formula status
prepare_milk_data <- function(post_nat, crb){
  
  # Breastfeeding ----
  bf_data <- select(post_nat, ident, ddn, contains("cy1ebb1"), contains("cy1ebb2"), cy1lab1_q01 : cy1lab1_q02)
  bf_data$ddn <- ymd(bf_data$ddn)
  
  # add sample date ----
  bf_data$ident <- as.character(bf_data$ident)
  crb$ident <- as.character(crb$ident)
  m2_dates <- crb %>% 
    filter(period == "M2") %>% 
    select(ident, sample_date) %>% 
    rename(sample_date_m2 = sample_date)
  y1_dates <- crb %>% 
    filter(period == "Y1") %>% 
    select(ident, sample_date) %>% 
    rename(sample_date_y1 = sample_date)
  
  bf_data <- left_join(bf_data, m2_dates, by = "ident")
  bf_data <- left_join(bf_data, y1_dates, by = "ident")
  
  # Manual correction----
  ###################################################################-
  # Export all questions where two age categories were filled
  # source("data/data_preparation/bf_formula_two_age_cat_filled_instead_of_one.R")
  # At 17/12/2018
  # 48 cases
  # Yoann said that they may have entered 2 months and 2 weeks instead of 6 weeks
  # Only one remaining case that seems inconsistent: 
  # id = 22368 filled 2 months and 5 weeks for the breastfeeding end date
  # She also indicates starting formula at 5 months and 1 week
  # There probably was an inversion between week and month of breastfeeding end
  ###################################################################-
  
  bf_data$cy1ebb2_q01p1[bf_data$ident == "22368"] <- 5
  bf_data$cy1ebb2_q01p2[bf_data$ident == "22368"] <- 2
  
  # * bf_start_date ----
  bf_data$bf_start_date <- bf_data$ddn
  
  # * bf_end_date ----
  
  ###################################################################-
  # Here we compute the age of end of breastfeeding using the two questionnaires 
  # @M2 and @Y1 and the catch up questionnaire CY1LAB1
  # Step1: we look at cy1ebb1_q01 and cy1ebb2_q01 = ever breastfed
  # If never breastfed: bf start date = bf end date = birth date
  # If breastfed then we compute bf end date
  # Step2: to compute bf end date we look at cy1ebb1_q01p3 and cy1ebb2_q01p3 = end date
  # Step3: for those with no date we look at cy1ebb1_q01p1, cy1ebb1_q01p2, cy1ebb2_q01p1 & cy1ebb2_q01p1 = end age
  # This means that when both date and age are provided, date is considered correct
  # Step4: catch up questionnaire
  #
  # NOTE: 1 unit is added when breastfeeding or formula end age given in weeks/months to include 
  # the whole period. Eg if child born on 01/01/N and breasfeeding stopped at "12 months" we 
  # decided that breastfeeding stop date == 01/02/N+1 and not 01/01/N+1 because we don't know 
  # when in the 12th month the breastfeeding actually stopped. Not doing so caused us to be too 
  # restrictive
  ##################################################################-
  
  # Add 1 unit when end age given in days/weeks/months (cf. previous NOTE)
  bf_data$cy1ebb1_q01p1 <- bf_data$cy1ebb1_q01p1 + 1
  bf_data$cy1ebb1_q01p2 <- bf_data$cy1ebb1_q01p2 + 1
  bf_data$cy1ebb1_q02p6 <- bf_data$cy1ebb1_q02p6 + 1
  bf_data$cy1ebb1_q02p7 <- bf_data$cy1ebb1_q02p7 + 1
  bf_data$cy1ebb2_q01p1 <- bf_data$cy1ebb2_q01p1 + 1
  bf_data$cy1ebb2_q01p2 <- bf_data$cy1ebb2_q01p2 + 1
  bf_data$cy1ebb2_q02p6 <- bf_data$cy1ebb2_q02p6 + 1
  bf_data$cy1ebb2_q02p7 <- bf_data$cy1ebb2_q02p7 + 1
  
  # bf_end_date: Init
  bf_data$bf_end_date <- ymd(NA)
  
  # bf_end_date: Step1
  bf_data$bf_end_date <- if_else(!is.na(bf_data$cy1ebb1_q01) &
                                   bf_data$cy1ebb1_q01 == "0",
                                 bf_data$bf_start_date,
                                 bf_data$bf_end_date) # 26 new dates
  
  # bf_end_date: Step2
  bf_data$bf_end_date <- if_else(is.na(bf_data$bf_end_date) &
                                   !is.na(bf_data$cy1ebb1_q01p3),
                                 bf_data$cy1ebb1_q01p3,
                                 bf_data$bf_end_date) # 19 new dates
  
  bf_data$bf_end_date <- if_else(is.na(bf_data$bf_end_date) &
                                   !is.na(bf_data$cy1ebb2_q01p3),
                                 bf_data$cy1ebb2_q01p3,
                                 bf_data$bf_end_date) # 36 new dates 
  
  # bf_end_date: Step3
  # For age elements where at least one is completed all NAs are set to 0 
  # like that the values can be summed.
  id_list_m2 <- bf_data$ident[!is.na(bf_data$cy1ebb1_q01p1) | !is.na(bf_data$cy1ebb1_q01p2)]
  bf_data$cy1ebb1_q01p1[bf_data$ident %in% id_list_m2 & is.na(bf_data$cy1ebb1_q01p1)] <- 0 # 11 corrections
  bf_data$cy1ebb1_q01p2[bf_data$ident %in% id_list_m2 & is.na(bf_data$cy1ebb1_q01p2)] <- 0 # 38 corrections
  
  bf_data$bf_end_date <- if_else(is.na(bf_data$bf_end_date) &
                                   (!is.na(bf_data$cy1ebb1_q01p1) | 
                                      !is.na(bf_data$cy1ebb1_q01p2)),
                                 bf_data$ddn + months(bf_data$cy1ebb1_q01p1) + weeks(bf_data$cy1ebb1_q01p2),
                                 bf_data$bf_end_date) # 31 new dates
  
  id_list_y1 <- bf_data$ident[!is.na(bf_data$cy1ebb2_q01p1) | !is.na(bf_data$cy1ebb2_q01p2)]
  bf_data$cy1ebb2_q01p1[bf_data$ident %in% id_list_y1 & is.na(bf_data$cy1ebb2_q01p1)] <- 0 # 14 corrections
  bf_data$cy1ebb2_q01p2[bf_data$ident %in% id_list_y1 & is.na(bf_data$cy1ebb2_q01p2)] <- 0 # 251 corrections
  
  bf_data$bf_end_date <- if_else(is.na(bf_data$bf_end_date) &
                                   (!is.na(bf_data$cy1ebb2_q01p1) | 
                                      !is.na(bf_data$cy1ebb2_q01p2)),
                                 bf_data$ddn + months(bf_data$cy1ebb2_q01p1) + weeks(bf_data$cy1ebb2_q01p2),
                                 bf_data$bf_end_date) # 201 new dates
  
  # bf_end_date: Step4
  bf_data$bf_end_date <- if_else(is.na(bf_data$bf_end_date) &
                                   (!is.na(bf_data$cy1lab1_q01p3)),
                                 bf_data$ddn + months(bf_data$cy1lab1_q01p3),
                                 bf_data$bf_end_date) # 32 new dates
  
  # * bf_m2 ----
  # binary variables: was breastfeeding @M2 sample and @Y1 sample
  
  # bf_m2
  bf_data$bf_m2 <- bf_data$bf_end_date > bf_data$sample_date_m2
  
  # Correct using the "still breastfeeding" @M2 variable
  bf_data$bf_m2 <- if_else(!is.na(bf_data$cy1ebb1_q01p4) &
                             bf_data$cy1ebb1_q01p4 == "1",
                           TRUE, bf_data$bf_m2)
  
  # Correct using the "still breastfeeding" @Y1 variable
  # For explanations see prev paragraph
  bf_data$bf_m2 <- if_else(!is.na(bf_data$cy1ebb2_q01p4) &
                             bf_data$cy1ebb2_q01p4 == "1",
                           TRUE, bf_data$bf_m2)
  
  # * bf_y1 ----
  # bf_y1
  bf_data$bf_y1 <- bf_data$bf_end_date > bf_data$sample_date_y1
  
  # Correct using the "still breastfeeding" @Y1 variable
  # For explanations see prev paragraph
  bf_data$bf_y1 <- if_else(!is.na(bf_data$cy1ebb2_q01p4) &
                             bf_data$cy1ebb2_q01p4 == "1",
                           TRUE, bf_data$bf_y1)
  
  # Correct usinf didn't breastfeed since M2
  bf_data$bf_y1 <- if_else(is.na(bf_data$bf_y1) &
                             !is.na(bf_data$cy1ebb2_q01) &
                             bf_data$cy1ebb2_q01 == "0",
                           FALSE, bf_data$bf_y1)
  
  # If bf was FALSE at M2 it has to be false at Y1
  # (this shouldn't correct any, but helps me to flag eventual incoherences)
  bf_data$bf_y1[!is.na(bf_data$bf_m2) & !bf_data$bf_m2] <- FALSE
  
  # Formula ----
  # * formula_start_date ----
  # Step 1: Look at date
  # Step 2: Look at age
  # Step 3: catch up data
  
  # formula_start_date: Init
  bf_data$formula_start_date <-  ymd(NA)
  
  # formula_start_date: Step1
  bf_data$formula_start_date <- if_else(!is.na(bf_data$cy1ebb1_q02p4),
                                        bf_data$cy1ebb1_q02p4,
                                        bf_data$formula_start_date) # 42 new dates
  
  bf_data$formula_start_date <- if_else(is.na(bf_data$formula_start_date) &
                                          !is.na(bf_data$cy1ebb2_q02p4),
                                        bf_data$cy1ebb2_q02p4,
                                        bf_data$formula_start_date) # 31 new dates
  
  # formula_start_date: Step2
  # see bf_end_date Step3 for explanations
  id_list_m2 <- bf_data$ident[!is.na(bf_data$cy1ebb1_q02p1) | !is.na(bf_data$cy1ebb1_q02p2) | !is.na(bf_data$cy1ebb1_q02p3)]
  bf_data$cy1ebb1_q02p1[bf_data$ident %in% id_list_m2 & is.na(bf_data$cy1ebb1_q02p1)] <- 0 # 54 corrections
  bf_data$cy1ebb1_q02p2[bf_data$ident %in% id_list_m2 & is.na(bf_data$cy1ebb1_q02p2)] <- 0 # 111 corrections
  bf_data$cy1ebb1_q02p3[bf_data$ident %in% id_list_m2 & is.na(bf_data$cy1ebb1_q02p3)] <- 0 # 121 corrections
  
  bf_data$formula_start_date <- if_else(is.na(bf_data$formula_start_date) &
                                          bf_data$ident %in% id_list_m2,
                                        bf_data$ddn + months(bf_data$cy1ebb1_q02p2) + weeks(bf_data$cy1ebb1_q02p3) + days(bf_data$cy1ebb1_q02p1),
                                        bf_data$formula_start_date) # 135 new dates
  
  id_list_y1 <- bf_data$ident[!is.na(bf_data$cy1ebb2_q02p1) | !is.na(bf_data$cy1ebb2_q02p2) | !is.na(bf_data$cy1ebb2_q02p3)]
  bf_data$cy1ebb2_q02p1[bf_data$ident %in% id_list_y1 & is.na(bf_data$cy1ebb2_q02p1)] <- 0 # 232 corrections
  bf_data$cy1ebb2_q02p2[bf_data$ident %in% id_list_y1 & is.na(bf_data$cy1ebb2_q02p2)] <- 0 # 94 corrections
  bf_data$cy1ebb2_q02p3[bf_data$ident %in% id_list_y1 & is.na(bf_data$cy1ebb2_q02p3)] <- 0 # 274 corrections
  
  bf_data$formula_start_date <- if_else(is.na(bf_data$formula_start_date) &
                                          bf_data$ident %in% id_list_y1,
                                        bf_data$ddn + months(bf_data$cy1ebb2_q02p2) + weeks(bf_data$cy1ebb2_q02p3) + days(bf_data$cy1ebb2_q02p1),
                                        bf_data$formula_start_date) # 36 new dates
  
  # formula_start_date: Step3
  bf_data$formula_start_date <- if_else(is.na(bf_data$formula_start_date) &
                                          !is.na(bf_data$cy1lab1_q01p5),
                                        bf_data$ddn + months(bf_data$cy1lab1_q01p5),
                                        bf_data$formula_start_date) # 4 new dates
  
  # * formula_end_date ----
  # Step 1: Look at date
  # Step 2: Look at age
  
  # formula_end_date: Init
  bf_data$formula_end_date <-  ymd(NA)
  
  # formula_end_date: Step1
  bf_data$formula_end_date <- if_else(!is.na(bf_data$cy1ebb1_q02p8),
                                      bf_data$cy1ebb1_q02p8,
                                      bf_data$formula_end_date) # 42 new dates
  
  bf_data$formula_end_date <- if_else(is.na(bf_data$formula_start_date) &
                                        !is.na(bf_data$cy1ebb2_q02p8),
                                      bf_data$cy1ebb2_q02p8,
                                      bf_data$formula_end_date) # 31 new dates
  
  # formula_end_date: Step2
  # see bf_end_date Step3 for explanations
  id_list_m2 <- bf_data$ident[!is.na(bf_data$cy1ebb1_q02p6) | !is.na(bf_data$cy1ebb1_q02p7)]
  bf_data$cy1ebb1_q02p6[bf_data$ident %in% id_list_m2 & is.na(bf_data$cy1ebb1_q02p6)] <- 0 # 8 corrections
  bf_data$cy1ebb1_q02p7[bf_data$ident %in% id_list_m2 & is.na(bf_data$cy1ebb1_q02p7)] <- 0 # 11 corrections
  
  bf_data$formula_end_date <- if_else(is.na(bf_data$formula_end_date) &
                                        bf_data$ident %in% id_list_m2,
                                      bf_data$ddn + months(bf_data$cy1ebb1_q02p6) + weeks(bf_data$cy1ebb1_q02p7),
                                      bf_data$formula_end_date) # 19 new dates
  
  id_list_y1 <- bf_data$ident[!is.na(bf_data$cy1ebb2_q02p6) | !is.na(bf_data$cy1ebb2_q02p7)]
  bf_data$cy1ebb2_q02p6[bf_data$ident %in% id_list_y1 & is.na(bf_data$cy1ebb2_q02p6)] <- 0 # 0 corrections
  bf_data$cy1ebb2_q02p7[bf_data$ident %in% id_list_y1 & is.na(bf_data$cy1ebb2_q02p7)] <- 0 # 31 corrections
  
  bf_data$formula_end_date <- if_else(is.na(bf_data$formula_end_date) &
                                        bf_data$ident %in% id_list_y1,
                                      bf_data$ddn + months(bf_data$cy1ebb2_q02p6) + weeks(bf_data$cy1ebb2_q02p7),
                                      bf_data$formula_end_date) # 23 new dates
  
  # * formula_m2 ---- 
  bf_data$formula_m2 <- bf_data$formula_start_date < bf_data$sample_date_m2 &
    bf_data$formula_end_date > bf_data$sample_date_m2 # 47 with info
  
  # correct if never used and questionnaire date > sample date
  # M2 quest
  bf_data$formula_m2[is.na(bf_data$formula_m2) &
                       !is.na(bf_data$cy1ebb1_q02) & 
                       bf_data$cy1ebb1_q02 == "0"] <- FALSE # 21 more
  
  # Y1 quest
  bf_data$formula_m2[is.na(bf_data$formula_m2) &
                       !is.na(bf_data$cy1ebb2_q02) & 
                       bf_data$cy1ebb2_q02 == "0"] <- FALSE # 5 more
  
  # correct if start date < sample date and still using and questionnaire date > sample date
  # two variables for the same information cy1ebb1_q01p5 and cy1ebb1_q01p9
  # Y1 quest
  # with cy1ebb2_q01p5
  bf_data$formula_m2[is.na(bf_data$formula_m2) &
                       !is.na(bf_data$cy1ebb1_q02p5) &
                       bf_data$cy1ebb1_q02p5 == "1" ] <- TRUE # 0 more
  
  # with cy1ebb2_q01p9
  bf_data$formula_m2[is.na(bf_data$formula_m2) &
                       !is.na(bf_data$cy1ebb1_q02p9) &
                       bf_data$cy1ebb1_q02p9 == "1"] <- TRUE # 14 more
  
  # Y1 quest + added condition that formula start before m2 sample
  # with cy1ebb2_q02p5
  bf_data$formula_m2[is.na(bf_data$formula_m2) &
                       !is.na(bf_data$formula_start_date) &
                       !is.na(bf_data$cy1ebb2_q02p5) &
                       !is.na(bf_data$sample_date_m2) &
                       bf_data$cy1ebb2_q02p5 == "1" &
                       bf_data$formula_start_date < bf_data$sample_date_m2] <- TRUE # 0 more
  
  # with cy1ebb2_q02p9
  bf_data$formula_m2[is.na(bf_data$formula_m2) &
                       !is.na(bf_data$formula_start_date) &
                       !is.na(bf_data$cy1ebb2_q02p9) &
                       !is.na(bf_data$sample_date_m2) &
                       bf_data$cy1ebb2_q02p9 == "1" &
                       bf_data$formula_start_date < bf_data$sample_date_m2] <- TRUE # 48 more
  
  # * formula_y1 ----
  bf_data$formula_y1 <- bf_data$formula_start_date < bf_data$sample_date_y1 &
    bf_data$formula_end_date > bf_data$sample_date_y1 # 17 with info
  
  # correct if never used
  # Y1 quest
  bf_data$formula_y1[is.na(bf_data$formula_y1) &
                       !is.na(bf_data$cy1ebb2_q02) & 
                       bf_data$cy1ebb2_q02 == "0"] <- FALSE
  
  # correct if start date < sample date and still using and questionnaire date > sample date
  # two variables for the same information cy1ebb1_q01p5 and cy1ebb1_q01p9
  
  # Y1 quest
  # with cy1ebb2_q02p5
  bf_data$formula_y1[is.na(bf_data$formula_y1) &
                       !is.na(bf_data$cy1ebb2_q02p5) &
                       bf_data$cy1ebb2_q02p5 == "1"] <- TRUE
  
  # with cy1ebb2_q02p9
  bf_data$formula_y1[is.na(bf_data$formula_y1) &
                       !is.na(bf_data$cy1ebb2_q02p9) &
                       bf_data$cy1ebb2_q02p9 == "1"] <- TRUE
  
  # Set to NA when no sample 
  bf_data$formula_m2[is.na(bf_data$sample_date_m2)] <- NA
  bf_data$formula_y1[is.na(bf_data$sample_date_y1)] <- NA
  
  ##################################################################-
  # Export breastfeeding/formula inconsistencies
  # ie cases where no BF & no formula @M2, missing either formula 
  # or breastfeeding and not the other @m2 or @y1
  # tab <- bf_data %>%
  #   filter(!(is.na(sample_date_m2) & is.na(sample_date_y1))) %>%
  #   filter((!bf_m2 & !formula_m2) |
  #            (!is.na(bf_m2) & is.na(formula_m2)) |
  #            (is.na(bf_m2) & !is.na(formula_m2)) |
  #            (!is.na(bf_y1) & is.na(formula_y1) & !is.na(sample_date_y1)) |
  #            (is.na(bf_y1) & !is.na(formula_y1) & !is.na(sample_date_y1))) %>%
  #   select(ident, ddn, sample_date_m2, sample_date_y1, bf_start_date, bf_end_date, formula_start_date,
  #          formula_end_date, bf_m2, formula_m2, bf_y1, formula_y1)
  # 
  #write_csv(tab, "data/data_preparation/data_verifications/bf_formula_inconsistencies.csv")
  ##################################################################-
  
  # Milk type ----
  # * milk_m2 ----
  bf_data$milk_m2 <- NA
  bf_data$milk_m2 <- ifelse(!is.na(bf_data$formula_m2) &
                              !is.na(bf_data$bf_m2) &
                              !bf_data$formula_m2 &
                              bf_data$bf_m2,
                            "breastfeeding only", 
                            bf_data$milk_m2)
  
  bf_data$milk_m2 <- ifelse(!is.na(bf_data$formula_m2) &
                              !is.na(bf_data$bf_m2) &
                              bf_data$formula_m2 &
                              !bf_data$bf_m2,
                            "formula only", 
                            bf_data$milk_m2)
  
  bf_data$milk_m2 <- ifelse(!is.na(bf_data$formula_m2) &
                              !is.na(bf_data$bf_m2) &
                              bf_data$formula_m2 &
                              bf_data$bf_m2,
                            "breastfeeding & formula", 
                            bf_data$milk_m2)
  
  bf_data$milk_m2 <- ifelse(!is.na(bf_data$formula_m2) &
                              !is.na(bf_data$bf_m2) &
                              !bf_data$formula_m2 &
                              !bf_data$bf_m2,
                            "no breastfeeding or formula", 
                            bf_data$milk_m2)
  
  # * milk_y1 ----
  bf_data$milk_y1 <- NA
  bf_data$milk_y1 <- ifelse(!is.na(bf_data$formula_y1) &
                              !is.na(bf_data$bf_y1) &
                              !bf_data$formula_y1 &
                              bf_data$bf_y1,
                            "breastfeeding only", 
                            bf_data$milk_y1)
  
  bf_data$milk_y1 <- ifelse(!is.na(bf_data$formula_y1) &
                              !is.na(bf_data$bf_y1) &
                              bf_data$formula_y1 &
                              !bf_data$bf_y1,
                            "formula only", 
                            bf_data$milk_y1)
  
  bf_data$milk_y1 <- ifelse(!is.na(bf_data$formula_y1) &
                              !is.na(bf_data$bf_y1) &
                              bf_data$formula_y1 &
                              bf_data$bf_y1,
                            "breastfeeding & formula", 
                            bf_data$milk_y1)
  
  bf_data$milk_y1 <- ifelse(!is.na(bf_data$formula_y1) &
                              !is.na(bf_data$bf_y1) &
                              !bf_data$formula_y1 &
                              !bf_data$bf_y1,
                            "no breastfeeding or formula", 
                            bf_data$milk_y1)
  # Milk manual corrections (data verified in the archives on the food daily diary with Sarah on 08/03/19)
  # For those that were computed as no breastfeeding or formula
  bf_data$milk_m2[bf_data$ident == 20716] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 14895] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 16439] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 16971] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 17154] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 17537] <- "breastfeeding & formula"
  bf_data$milk_y1[bf_data$ident == 18553] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 18725] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 18925] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 20098] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 20502] <- "breastfeeding & formula"
  bf_data$milk_y1[bf_data$ident == 22809] <- "formula only"
  # For those that were computed as missing
  bf_data$milk_m2[bf_data$ident == 14177] <- "formula only"
  bf_data$milk_m2[bf_data$ident == 14895] <- "formula only"
  bf_data$milk_m2[bf_data$ident == 16403] <- "breastfeeding only"
  bf_data$milk_m2[bf_data$ident == 17143] <- "breastfeeding only"
  bf_data$milk_m2[bf_data$ident == 20541] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 15736] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 15813] <- "breastfeeding only"
  bf_data$milk_y1[bf_data$ident == 16115] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 16786] <- "breastfeeding only"
  bf_data$milk_y1[bf_data$ident == 17700] <- "formula only"
  bf_data$milk_y1[bf_data$ident == 17764] <- "breastfeeding & formula"
  bf_data$milk_y1[bf_data$ident == 17886] <- "formula only"
  
  ##################################################################-
  # we have two that were neither breastfed nor had formula
  # for one we were able to confirm in the archive it is the case (ident = 19868)
  # the other was not found in the archives (ident = 20640)
  # so we decided to recode the breastfeeding variable as bf/formula/other and
  # mix (bf&formula) and (no bf & no formula) and to re-run the analysis without 
  # the two that  had no bf or no formula
  ##################################################################-
  
  bf_data$milk_m2 <- fct_recode(bf_data$milk_m2, other = "breastfeeding & formula")
  bf_data$milk_y1 <- fct_recode(bf_data$milk_y1, other = "breastfeeding & formula")
  bf_data$milk_y1 <- fct_recode(bf_data$milk_y1, other = "no breastfeeding or formula")
  
  # export vars
  bf_data <- bf_data %>%
    select(ident, bf_m2, bf_y1, formula_m2, formula_y1, milk_m2, milk_y1)
  
  # wide to long ----
  bf_final <- period_wide_to_long(bf_data)
  
  return(bf_final)
}
